package com.carlos.star_wars.service.mision;

import com.carlos.star_wars.model.Capitan;
import com.carlos.star_wars.model.Mision;

import java.util.List;

public interface IMisionService {

    List<Mision> getAll();

    List<Mision> getAllByLideres(List<Capitan> lideres);

    Mision save(Mision mision) throws Exception;

}
