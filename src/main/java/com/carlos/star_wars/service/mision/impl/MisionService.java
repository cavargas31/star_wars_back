package com.carlos.star_wars.service.mision.impl;

import com.carlos.star_wars.model.Capitan;
import com.carlos.star_wars.model.Mision;
import com.carlos.star_wars.repository.MisionRepository;
import com.carlos.star_wars.service.mision.IMisionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class MisionService implements IMisionService {

    private final MisionRepository misionRepository;

    @Autowired
    public MisionService(MisionRepository misionRepository) {
        this.misionRepository = misionRepository;
    }

    @Override
    public List<Mision> getAll() {
        return this.misionRepository.findAll();
    }

    @Override
    public List<Mision> getAllByLideres(List<Capitan> lideres) {
        return this.misionRepository.findDistinctByLideresIn(lideres);
    }

    @Override
    public Mision save(Mision mision) throws RuntimeException {
        if (isValid(mision.getLideres().size(),
                mision.getTripulacion().intValue(),
                mision.getNave().getTripulacion().intValue(),
                mision.getNave().getPasajeros().intValue(),
                mision.getLideres())) {
            mision.setFechaFinal(calculateFechaFinal(mision));
        } else {
            throw new RuntimeException("El cuerpo del servicio no es valido");
        }

        return this.misionRepository.save(mision);
    }
    
    private Date calculateFechaFinal(Mision mision) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mision.getFechaInicio());
        calendar.add(Calendar.HOUR, calculateMisionDuration(mision));

        log.info("Duracion de la mision: " + calculateMisionDuration(mision));
        return calendar.getTime();
    }

    private int calculateMisionDuration(Mision mision) {
        double distanceCrewKm = (mision.getLideres().size() * 100) + (mision.getTripulacion().intValue() * 10);
        double distanceMisionKm = mision.getRutas()
                .stream()
                .mapToDouble(d -> d.getDiametro().doubleValue())
                .sum();

        return (int) Math.ceil((1 * distanceMisionKm) / distanceCrewKm);
    }

    private boolean isValid(int capitanesMision, int tripulacionMision,
                            int tripulacionNave, int pasajerosNave, List<Capitan> lideres) {
        return capitanesMision + tripulacionMision >= tripulacionNave &&
                capitanesMision + tripulacionMision <= tripulacionNave + pasajerosNave &&
                isLiderInactive(lideres);
    }

    private boolean isLiderInactive(List<Capitan> lideres) {
        return getAllByLideres(lideres).stream().allMatch(m -> m.getFechaFinal().compareTo(new Date()) < 0);
    }

}
