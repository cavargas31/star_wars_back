package com.carlos.star_wars.service.nave;

import com.carlos.star_wars.model.Nave;

import java.util.List;

public interface INaveService {

    List<Nave> getAll();

}
