package com.carlos.star_wars.service.nave.impl;

import com.carlos.star_wars.model.Nave;
import com.carlos.star_wars.repository.NaveRepository;
import com.carlos.star_wars.service.nave.INaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NaveService implements INaveService {

    private final NaveRepository naveRepository;

    @Autowired
    public NaveService(NaveRepository naveRepository) {
        this.naveRepository = naveRepository;
    }

    @Override
    public List<Nave> getAll() {
        return this.naveRepository.findAll();
    }

}
