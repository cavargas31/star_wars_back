package com.carlos.star_wars.service.capitan;

import com.carlos.star_wars.model.Capitan;

import java.util.List;

public interface ICapitanService {

    List<Capitan> getAll();

}
