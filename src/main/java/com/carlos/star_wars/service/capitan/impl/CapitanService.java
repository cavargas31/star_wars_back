package com.carlos.star_wars.service.capitan.impl;

import com.carlos.star_wars.model.Capitan;
import com.carlos.star_wars.repository.CapitanRepository;
import com.carlos.star_wars.service.capitan.ICapitanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CapitanService implements ICapitanService {

    private final CapitanRepository capitanRepository;

    @Autowired
    public CapitanService(CapitanRepository capitanRepository) {
        this.capitanRepository = capitanRepository;
    }

    @Override
    public List<Capitan> getAll() {
        return this.capitanRepository.findAll();
    }

}
