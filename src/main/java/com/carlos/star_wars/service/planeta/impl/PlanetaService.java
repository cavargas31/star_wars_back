package com.carlos.star_wars.service.planeta.impl;

import com.carlos.star_wars.model.Planeta;
import com.carlos.star_wars.repository.PlanetaRepository;
import com.carlos.star_wars.service.planeta.IPlanetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanetaService implements IPlanetaService {

    private final PlanetaRepository planetaRepository;

    @Autowired
    public PlanetaService(PlanetaRepository planetaRepository) {
        this.planetaRepository = planetaRepository;
    }

    @Override
    public List<Planeta> getAll() {
        return this.planetaRepository.findAll();
    }

}
