package com.carlos.star_wars.service.planeta;

import com.carlos.star_wars.model.Planeta;

import java.util.List;

public interface IPlanetaService {

    List<Planeta> getAll();

}
