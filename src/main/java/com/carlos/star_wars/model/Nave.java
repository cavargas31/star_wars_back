package com.carlos.star_wars.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "NAVES", schema = "dbo")
public class Nave {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nombre", nullable = false, length = 150)
    private String nombre;
    @Column(name = "tripulacion", nullable = false, precision = 10)
    private BigDecimal tripulacion;
    @Column(name = "pasajeros", nullable = false, precision = 10)
    private BigDecimal pasajeros;

}
