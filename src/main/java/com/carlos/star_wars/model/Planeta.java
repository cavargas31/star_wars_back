package com.carlos.star_wars.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PLANETAS", schema = "dbo")
public class Planeta {

    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "nombre", nullable = false, length = 150)
    private String nombre;
    @Column(name = "diametro", nullable = false, precision = 10, scale = 2)
    private BigDecimal diametro;

}
