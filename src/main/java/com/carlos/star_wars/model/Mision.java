package com.carlos.star_wars.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MISIONES")
public class Mision {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "fecha_inicio", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fechaInicio;
    @JoinColumn(name = "id_nave", nullable = false, referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "FK_MISIONES.id_nave"))
    @ManyToOne(targetEntity = Nave.class, optional = false, cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    private Nave nave;
    @Column(name = "tripulacion", nullable = false, precision = 10)
    private BigDecimal tripulacion;
    @Column(name = "fecha_final", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fechaFinal;
    @JoinTable(name = "RUTAS",
            joinColumns = @JoinColumn(name = "id_mision", nullable = false, referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "FK_RUTAS.id_mision")),
            inverseJoinColumns = @JoinColumn(name = "id_planeta", nullable = false, referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "FK_RUTAS.id_planeta")))
    @ManyToMany(targetEntity = Planeta.class,
            cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH})
    private List<Planeta> rutas;
    @JoinTable(name = "LIDERES",
            joinColumns = @JoinColumn(name = "id_mision", nullable = false, referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "FK_LIDERES.id_mision")),
            inverseJoinColumns = @JoinColumn(name = "id_capitan", nullable = false, referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "FK_LIDERES.id_capitan")))
    @ManyToMany(targetEntity = Capitan.class,
            cascade = {CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH})
    private List<Capitan> lideres;

}
