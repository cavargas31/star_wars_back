package com.carlos.star_wars.controller.planeta;

import com.carlos.star_wars.model.Planeta;
import com.carlos.star_wars.service.planeta.IPlanetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "star_wars/v1/planetas")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET})
public class PlanetaController {

    private final IPlanetaService planetaService;

    @Autowired
    public PlanetaController(IPlanetaService planetaService) {
        this.planetaService = planetaService;
    }

    @GetMapping
    public ResponseEntity<List<Planeta>> findAll() {
        return ResponseEntity.ok().body(this.planetaService.getAll());
    }

}
