package com.carlos.star_wars.controller.mision;

import com.carlos.star_wars.model.Capitan;
import com.carlos.star_wars.model.Mision;
import com.carlos.star_wars.service.mision.IMisionService;
import com.sun.istack.Nullable;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "star_wars/v1/misiones")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET})
@Tag(name = "Misiones", description = "Servicios API Misiones Star Wars")
public class MisionController {

    private final IMisionService misionService;

    @Autowired
    public MisionController(IMisionService misionService) {
        this.misionService = misionService;
    }

    @Operation(summary = "Lista todas las misiones y filtra por uno o más capitanes que estén en la misión")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion Exitosa"),
            @ApiResponse(responseCode = "400", description = "Mala peticion en los capitanes")})
    @PostMapping("/listar")
    public ResponseEntity<List<Mision>> findAll(@RequestBody(required = false) @Nullable List<Capitan> lideres) {
        return lideres == null || lideres.isEmpty() ? ResponseEntity.ok().body(this.misionService.getAll())
                : ResponseEntity.ok().body(this.misionService.getAllByLideres(lideres));
    }

    @Operation(summary = "Crea misiones espaciales")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Mision Creada"),
            @ApiResponse(responseCode = "400", description = "Invalida entrada")})
    @PostMapping
    public ResponseEntity<Mision> create(@RequestBody Mision mision) throws Exception {
        return ResponseEntity.ok().body(this.misionService.save(mision));
    }

}
