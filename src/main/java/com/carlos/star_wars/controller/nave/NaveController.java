package com.carlos.star_wars.controller.nave;

import com.carlos.star_wars.model.Nave;
import com.carlos.star_wars.service.nave.INaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "star_wars/v1/naves")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET})
public class NaveController {

    private INaveService naveService;

    @Autowired
    public NaveController(INaveService naveService) {
        this.naveService = naveService;
    }

    @GetMapping
    public ResponseEntity<List<Nave>> findAll() {
        return ResponseEntity.ok().body(this.naveService.getAll());
    }

}
