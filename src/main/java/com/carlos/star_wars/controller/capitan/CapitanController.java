package com.carlos.star_wars.controller.capitan;

import com.carlos.star_wars.model.Capitan;
import com.carlos.star_wars.service.capitan.ICapitanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "star_wars/v1/capitanes")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT, RequestMethod.GET})
public class CapitanController {

    private ICapitanService capitanService;

    @Autowired
    public CapitanController(ICapitanService capitanService) {
        this.capitanService = capitanService;
    }

    @GetMapping
    public ResponseEntity<List<Capitan>> findAll() {
        return ResponseEntity.ok().body(this.capitanService.getAll());
    }

}
