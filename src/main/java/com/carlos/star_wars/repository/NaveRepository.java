package com.carlos.star_wars.repository;

import com.carlos.star_wars.model.Nave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NaveRepository extends JpaRepository<Nave, Integer> {



}
