package com.carlos.star_wars.repository;

import com.carlos.star_wars.model.Planeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetaRepository extends JpaRepository<Planeta, Integer> {



}
