package com.carlos.star_wars.repository;

import com.carlos.star_wars.model.Capitan;
import com.carlos.star_wars.model.Mision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface MisionRepository extends JpaRepository<Mision, Integer> {

    List<Mision> findDistinctByLideresIn(List<Capitan> lideres);

}
