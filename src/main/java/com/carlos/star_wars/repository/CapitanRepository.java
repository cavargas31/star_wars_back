package com.carlos.star_wars.repository;

import com.carlos.star_wars.model.Capitan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CapitanRepository extends JpaRepository<Capitan, Integer> {



}
