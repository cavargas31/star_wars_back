USE [DB-STARWARS]
GO


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
																				/****** DROP TABLES ******/
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[RUTAS]    Script Date: 26/11/2021 10:55:50 p.�m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RUTAS]') AND type in (N'U'))
DROP TABLE [dbo].[RUTAS]
GO

/****** Object:  Table [dbo].[LIDERES]    Script Date: 26/11/2021 10:55:50 p.�m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LIDERES]') AND type in (N'U'))
DROP TABLE [dbo].[LIDERES]
GO

/****** Object:  Table [dbo].[MISIONES]    Script Date: 26/11/2021 10:55:50 p.�m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MISIONES]') AND type in (N'U'))
DROP TABLE [dbo].[MISIONES]
GO

/****** Object:  Table [dbo].[PLANETAS]    Script Date: 26/11/2021 10:55:50 p.�m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PLANETAS]') AND type in (N'U'))
DROP TABLE [dbo].[PLANETAS]
GO

/****** Object:  Table [dbo].[CAPITANES]    Script Date: 26/11/2021 10:55:50 p.�m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CAPITANES]') AND type in (N'U'))
DROP TABLE [dbo].[CAPITANES]
GO

/****** Object:  Table [dbo].[NAVES]    Script Date: 26/11/2021 10:55:50 p.�m. ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NAVES]') AND type in (N'U'))
DROP TABLE [dbo].[NAVES]
GO


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
																				/****** CREATE TABLES ******/
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/** Object:  Table [dbo].[PLANETAS]    Script Date: 26/11/2021 10:55:50 p.�m. **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PLANETAS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](150) NOT NULL,
	[diametro] [numeric](10, 2) NOT NULL,
CONSTRAINT [PK_PLANETAS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
CONSTRAINT [UQ_PLANETAS.nombre] UNIQUE NONCLUSTERED 
(
	[nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


/** Object:  Table [dbo].[CAPITANES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CAPITANES](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](150) NOT NULL,
CONSTRAINT [PK_CAPITANES] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/** Object:  Table [dbo].[NAVES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NAVES](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](150) NOT NULL,
	[tripulacion] [numeric](10, 0) NOT NULL,
	[pasajeros] [numeric](10, 0) NOT NULL,
CONSTRAINT [PK_NAVES] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
CONSTRAINT [UQ_NAVES.nombre] UNIQUE NONCLUSTERED 
(
	[nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


/** Object:  Table [dbo].[MISIONES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MISIONES](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha_inicio] [datetime] NOT NULL,
	[id_nave] [int] NOT NULL,
	[tripulacion] [numeric](10, 0) NOT NULL,
	[fecha_final] [datetime] NULL,
CONSTRAINT [PK_MISIONES] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MISIONES] WITH CHECK ADD CONSTRAINT [FK_MISIONES.id_nave] FOREIGN KEY([id_nave])
REFERENCES [dbo].[NAVES] ([id])
GO


/** Object:  Table [dbo].[RUTAS]    Script Date: 26/11/2021 10:55:50 p.�m. **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RUTAS](
	[id_mision] [int] NOT NULL,
	[id_planeta] [int] NOT NULL,
CONSTRAINT [PK_RUTAS] PRIMARY KEY CLUSTERED 
(
	[id_mision], [id_planeta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[RUTAS] WITH CHECK ADD CONSTRAINT [FK_RUTAS.id_mision] FOREIGN KEY([id_mision])
REFERENCES [dbo].[MISIONES] ([id])
GO

ALTER TABLE [dbo].[RUTAS] WITH CHECK ADD CONSTRAINT [FK_RUTAS.id_planeta] FOREIGN KEY([id_planeta])
REFERENCES [dbo].[PLANETAS] ([id])
GO


/** Object:  Table [dbo].[LIDERES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LIDERES](
	[id_mision] [int] NOT NULL,
	[id_capitan] [int] NOT NULL,
CONSTRAINT [PK_LIDERES] PRIMARY KEY CLUSTERED 
(
	[id_mision], [id_capitan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LIDERES] WITH CHECK ADD CONSTRAINT [FK_LIDERES.id_mision] FOREIGN KEY([id_mision])
REFERENCES [dbo].[MISIONES] ([id])
GO

ALTER TABLE [dbo].[LIDERES] WITH CHECK ADD CONSTRAINT [FK_LIDERES.id_capitan] FOREIGN KEY([id_capitan])
REFERENCES [dbo].[CAPITANES] ([id])
GO


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
																				/****** INSERT DATA TABLES ******/
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/** Insert:  Table [dbo].[LIDERES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
INSERT INTO [dbo].[CAPITANES] ([nombre]) VALUES ('CARLOS')
INSERT INTO [dbo].[CAPITANES] ([nombre]) VALUES ('ANDR�S')
INSERT INTO [dbo].[CAPITANES] ([nombre]) VALUES ('DANIELA')

GO

/** Insert:  Table [dbo].[LIDERES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
INSERT INTO [dbo].[PLANETAS] ([nombre],[diametro])VALUES ('SATURNO',116460)
INSERT INTO [dbo].[PLANETAS] ([nombre],[diametro])VALUES ('VENUS',12104)
INSERT INTO [dbo].[PLANETAS] ([nombre],[diametro])VALUES ('TIERRA',12742)

GO

/** Insert:  Table [dbo].[LIDERES]    Script Date: 26/11/2021 10:55:50 p.�m. **/
INSERT INTO [dbo].[NAVES] ([nombre],[tripulacion],[pasajeros]) VALUES ('S�PER DESTRUCTOR ESTELAR',30,150)
INSERT INTO [dbo].[NAVES] ([nombre],[tripulacion],[pasajeros]) VALUES ('TANTIVE IV',10,60)
INSERT INTO [dbo].[NAVES] ([nombre],[tripulacion],[pasajeros]) VALUES ('RAZOR CRETS',5,30)

GO